/**
  ******************************************************************************
  * @file    raingauge_station.h
  * @author  CDOH Team
  * @brief   This file contains all the functions prototypes for the Rain Gauge station driver.
  ******************************************************************************
  */
#ifndef RAINGAUGE_STATION_H_
#define RAINGAUGE_STATION_H_

#include "stdint.h"
#include "sys_app.h"


/**
 * @defgroup Battery, Rain Gauge and UV
 */

#define BATT_ENABLE_PORT		GPIOB
#define BATT_ENABLE_PIN			GPIO_PIN_2
#define BATTERY_CHANNEL			ADC_CHANNEL_4


#define RAINGAUGE_PORT			GPIOB
#define RAINGAUGE_PIN			GPIO_PIN_14

#define UV_ENABLE_PORT	        GPIOB
#define UV_ENABLE_PIN		    GPIO_PIN_13
#define UV_CHANNEL		        ADC_CHANNEL_0


#define BATT_POWER    				1


#define DOWNLINK_RAINPORT        	5

#define RAIN_MEMORY_ADD				0x08080008


#define R1 	10		/*10 K resistor R1 in voltage divider*/
#define R2 	10		/*10 K resistor R2 in voltage divider*/

/**
 * Structure to store sensor data
 */
typedef struct {


	uint16_t rainfall;

    uint16_t batteryLevel;

    uint16_t totalrainfall;

    uint16_t downlinkRainfall;

    uint16_t uv;

}rainfallData_t;

/** @defgroup Rain Guage Station Exported_Functions_Peripheral Control functions
  * @brief    Peripheral Control functions
  */
uint16_t readBatteryLevel(void);

void rainGaugeTips();
uint16_t getRainfall();
uint16_t getTotalRainfall(uint8_t downlinkReceived);

void enable(uint8_t);
void disable(uint8_t);
void raingaugeStationInit();
void readRaingaugeStationParameters(rainfallData_t *sensor_data);
void raingaugeStationGPIO_Init();
void rainGaugeInterruptEnable();
uint16_t readAnalogUvValue(void);

#endif
